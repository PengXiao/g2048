/**
 * Controls the current game behavior, e.g, handles
 */
abstract class Game2048Controller {
  /**
   * Retrieve the current game grid
   */
  def getCurrentGame: GameGrid

  /**
   * Record which cell is the last newly added
   */
  var lastNewCell: GridCell = NonExistsCell

  /**
   * A move consists:
   * - move all grid cells to align to the target direction. merge if possible.
   * - generate a random cell if not full grid
   *
   * Note: this is expected to have side effect
   */
  def next(direction: Move): SimpleGameController

  /**
   * Move the current game and add a new random cell
   */
  def moveAndAdd(direction: Move): GameGrid =
    if (canMove(direction)) {
      val gridAfterMove = direction match {
        case Left => moveLeft
        case Right => moveRight
        case Down => moveDown
        case Up => moveUp
      }
      if (!gridAfterMove.full) {
        val newCell = GridCell.rand(gridAfterMove.emptyCells)
        lastNewCell = newCell
        gridAfterMove.update(newCell)
      } else {
        println("Game is full!")
        gridAfterMove
      }
    } else {
      println(s"I cannot take a move: $direction!")
      getCurrentGame
    }

  /**
   * Take an direction before adding a new cell
   */
  private def moveBeforeNewCell(direction: Move): GameGrid = direction match {
    case Left => moveLeft
    case Right => moveRight
    case Down => moveDown
    case Up => moveUp
  }

  /**
   * Merge the cell from head to tail, fold left
   */
  def mergeCells(cells: List[GridCell]): List[GridCell] = {
    // fold left with the empty cells
    def shuffle(cells: List[GridCell]): List[GridCell] = cells match {
      case a :: b :: tail => if (a.empty) {
        shuffle(a.merge(b) :: tail.map(cell => GridCell(cell.pos + (a.pos - b.pos), cell.value)))
      } else a :: shuffle(b :: tail)
      case List(a) if a.empty => Nil
      case _ => cells
    }

    // fold left and sum cells. leave an empty cell if a merge happens (this is to ensure no double counting)
    def merge(cells: List[GridCell]): List[GridCell] = cells match {
      case a :: b :: tail => if (a.canMerge(b)) a.merge(b) :: mergeCells(GridCell(b.pos, 0) :: tail) else a :: mergeCells(b :: tail)
      case _ => cells
    }

    /**
     * A merge can be seen as three operations:
     * - remove empty cells
     * - fold the cells to the left recursively; leave an empty cell at the current position
     * - remove empty cells again
     */
    shuffle(merge(shuffle(cells)))
  }

  /**
   * The logic is:
   * - group the grid in the direction we want to merge
   * - sort the cells in each group so that they are in the order of merging
   * - do merge for each group
   * - flatten the cells after merge
   * - create a new board when a move finishes
   */
  private def moveUp: GameGrid = {
    // group by col
    val cols = getCurrentGame.cells.groupBy(cell => cell.pos.col)
    val mergedCols = for {
      (colIdx, col) <- cols
      sortedCol = col.toList.sortWith((a, b) => a.pos.row < b.pos.row)
      mergedCol = mergeCells(sortedCol)
    } yield mergedCol
    GameGrid.initWithCells(mergedCols.flatten)
  }

  private def moveDown: GameGrid = {
    // group by col
    val cols = getCurrentGame.cells.groupBy(cell => cell.pos.col)
    val mergedCols = for {
      (colIdx, col) <- cols
      sortedCol = col.toList.sortWith((a, b) => a.pos.row > b.pos.row)
      mergedCol = mergeCells(sortedCol)
    } yield mergedCol
    GameGrid.initWithCells(mergedCols.flatten)
  }

  private def moveLeft: GameGrid = {
    // group by row
    val rows = getCurrentGame.cells.groupBy(cell => cell.pos.row)
    val mergedRows = for {
      (rowIdx, row) <- rows
      sortedRow = row.toList.sortWith((a, b) => a.pos.col < b.pos.col)
      mergedRow = mergeCells(sortedRow)
    } yield mergedRow
    GameGrid.initWithCells(mergedRows.flatten)
  }

  private def moveRight: GameGrid = {
    // group by row
    val rows = getCurrentGame.cells.groupBy(cell => cell.pos.row)
    val mergedRows = for {
      (rowIdx, row) <- rows
      sortedRow = row.toList.sortWith((a, b) => a.pos.col > b.pos.col)
      mergedRow = mergeCells(sortedRow)
    } yield mergedRow
    GameGrid.initWithCells(mergedRows.flatten)
  }

  /**
   * Determines if the current game cannot make any move
   */
  def deadGame: Boolean = !(canMove(Left) || canMove(Right) || canMove(Up) || canMove(Down))

  /**
   * Determines if the game can move in the direction
   */
  def canMove(direction: Move): Boolean = getCurrentGame != moveBeforeNewCell(direction)
}

/**
 * The move directions of the game
 */
trait Move

case object Left extends Move

case object Right extends Move

case object Up extends Move

case object Down extends Move