import java.util.Date

import scala.util.Random

object GameGrid {
  /**
   * 2048 is a 4x4 size board game
   */
  def SIZE = 4

  /**
   * Init the game grid
   */
  def init(numOfCell: Int): GameGrid = {
    var cells: Map[Pos, GridCell] = Map.empty
    // Seems low performance ... but should be OK
    while (cells.size != numOfCell) {
      val cell = GridCell.rand
      cells = cells.updated(cell.pos, cell)
    }
    initWithCells(cells.unzip._2)
  }

  /**
   * Init with a set of cells
   */
  def initWithCells(cells: Iterable[GridCell]): GameGrid = {
    val cellMap = cells.map(cell => cell.pos -> cell).toMap withDefault(pos => GridCell(pos, 0))
    val extendedCellMap = for {
      col <- 0 until SIZE
      row <- 0 until SIZE
      cell = cellMap(Pos(col, row))
    } yield cell
    GameGrid(extendedCellMap.toVector)
  }
}

/**
 * Represents the game grid board
 */
case class GameGrid(cells: Vector[GridCell]) {
  val totalCells = GameGrid.SIZE * GameGrid.SIZE
  require(cells.size == totalCells, s"The game board requires $totalCells cells")

  /**
   * Fetch a cell from the grid
   */
  def apply(pos: Pos): GridCell = gridMap.get(pos) match {
    case Some(cell) => cell
    case None => GridCell(pos, 0)
  }

  /**
   * Update a single cell of the grid
   */
  def update(cell: GridCell): GameGrid = {
    val idx = cells.indexWhere(_.pos == cell.pos)
    GameGrid(cells.updated(idx, cell))
  }
  
  /**
   * Determines whether this grid is full or not
   */
  def full: Boolean = emptyCells.size == 0

  /**
   * Return a vector of empty cells' positions
   */
  lazy val emptyCells: Vector[Pos] = cells.filter(_.empty).map(_.pos)

  /**
   * The Map representation of the grid
   */
  lazy val gridMap: Map[Pos, GridCell] = cells.map(cell => cell.pos -> cell).toMap

  /**
   * Get the string representation of the game grid in row based order
   */
  override def toString = "" + cells.filter(!_.empty).map(_.pos)
}

case class Pos(col: Int, row: Int) {
  def +(pos: Pos): Pos = Pos(col + pos.col, row + pos.row)
  def -(pos: Pos): Pos = Pos(col - pos.col, row - pos.row)

  override def toString = s"[$col,$row]"
}

/**
 * A grid cell represents a cell in the 2048 game board
 */
case class GridCell(pos: Pos, value: Int) {
  override def toString = s"$pos<$value>"

  /**
   * Merge with another cell, the position will be used is this's pos
   * @param that another cell
   * @return a new cell at the current position with summed value
   */
  def merge(that: GridCell): GridCell = if (canMerge(that)) GridCell(pos, value + that.value) else this

  /**
   * Determines whether this can be merged with that
   * A merge is possible if that has the same value
   * @param that the other cell
   * @return true if they can merge
   */
  def canMerge(that: GridCell): Boolean = pos != that.pos && (value == that.value || empty)

  def empty: Boolean = value == 0
}

object NonExistsCell extends GridCell(Pos(-1, -1), 0)

/**
 * Randomly generate a GridCell
 */
object GridCell {
  protected val seed = new Date().getTime
  private val random = new Random(seed)
  def rand: GridCell = GridCell(Pos(randIdx, randIdx), randVal)

  private def randIdx: Int = random.nextInt(GameGrid.SIZE)

  /**
   * The value should not be uni-distributed in the actual game tho
   *
   * random a value, either 2 or 4
   */
  private def randVal: Int = (random.nextInt(2) + 1) * 2

  /**
   * Randomize a cell within the given positions
   */
  def rand(emptyCells: Vector[Pos]): GridCell = GridCell(emptyCells(random.nextInt(emptyCells.size)), randVal)
}