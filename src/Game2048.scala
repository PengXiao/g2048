import scala.io.StdIn
import scala.util.Random

/**
 * A simple implementation determines how the current game status is stored
 * @param currentGame the currentGame board. Use this to init the game.
 */
class SimpleGameController(var currentGame: GameGrid) extends Game2048Controller {

  override def getCurrentGame: GameGrid = currentGame

  override def next(action: Move): SimpleGameController = {
    // Update the game reference each turn
    currentGame = super.moveAndAdd(action)
    this
  }

  // Randomizer for testing
  val random = new Random()

  def randomNext: SimpleGameController = {
    random.nextInt(4) match {
      case 0 => next(Up)
      case 1 => next(Down)
      case 2 => next(Left)
      case 3 => next(Right)
    }
  }
}

/**
 * The main program loop
 */
object Game2048 extends App {
  loop()

  /**
   * Get the string representation of the game grid in row based order
   *
   * eg:
   * [   2][    ][   2][    ]
   * [    ][    ][    ][    ]
   * [   2][    ][    ][1024]
   * [   4][    ][    ][    ]
   */
  def printGame(game: SimpleGameController): String = {
    for (rowIdx <- 0 until GameGrid.SIZE)
      yield {
        for {
          colIdx <- 0 until GameGrid.SIZE
          cell = game.getCurrentGame(Pos(colIdx, rowIdx))
          cellVal = cell.value
          cellStr = if (cellVal == 0) "[    ]" else if (cell == game.lastNewCell) f"{$cellVal%4d}" else f"[$cellVal%4d]"
        } yield cellStr
      }.foldLeft("")(_ + _)
  }.foldLeft("\n")(_ + _ + "\n")

  def loop(): Unit = {
    val game = new SimpleGameController(GameGrid.init(2))
    var moveId = 0
    do {
      println(moveId + "'s move" + printGame(game))

      case object InvalidMove extends Move

      try {
        val dir: Move = StdIn.readChar() match {
          case 'a' => Left
          case 'd' => Right
          case 'w' => Up
          case 's' => Down
          case _ => InvalidMove
        }
        if (dir != InvalidMove) {
          game.next(dir)
          moveId += 1
        }
      } catch {
        case ex: StringIndexOutOfBoundsException => // do nothing
        case otherEx: Throwable => throw otherEx
      }
    } while (!game.deadGame)

    println("Game Over!")
  }
}